import { capsToFront, numToBack, reorder } from './index'

describe('UNIT: Caps to front', ()=> {

    it('should move caps to front in hEll2O', ()=> {
        expect( capsToFront( 'hEll2O' ) ).toEqual( ['E', 'O', 'h', 'l', 'l', '2'] )
    })

    it('should move caps to front in  WoRd6SareFun', ()=> {
        const expected = ['W','R','S','F','o','d','6','a','r','e','u','n']
        expect( capsToFront( 'WoRd6SareFun' ) ).toEqual( expected )
    })

    it('should move caps to front in helloGOODBYE', ()=> {
        const expected = ['G', 'O', 'O', 'D', 'B', 'Y', 'E', 'h', 'e', 'l', 'l', 'o']
        expect( capsToFront( 'helloGOODBYE' ) ).toEqual( expected )
    })

    it('should move caps to fro in JaVaScRiPt', () => {
        const expected = ['J', 'V', 'S', 'R', 'P', 'a', 'a', 'c', 'i', 't']
        expect( capsToFront( 'JaVaScRiPt' ) ).toEqual( expected )
    })
})

describe('UNIT: Num to Back', ()=> {

    it("Moves nums to back ['E', 'O', 'h', 'l', 'l', '2']", ()=> {
        const source = ['E', 'O', 'h', 'l', 'l', '2']
        const expected = ['E', 'O', 'h', 'l', 'l', '2']
        expect( numToBack( source ) ).toEqual( expected )
    })

    it("moves to back ['W','R','S','F','o','d','6','a','r','e','u','n']", ()=> {
        const source = ['W','R','S','F','o','d','6','a','r','e','u','n']
        const expected = ['W','R','S','F','o','d','a','r','e','u','n','6']
        expect( numToBack( source ) ).toEqual( expected )
    })
})

describe('INT: Reorder Word', ()=> {
    it('should reorder wIldStu2Ff', ()=> {
        expect( reorder( 'wIldStu2Ff' ) ).toBe(  'ISFwldtuf2' )
    })

    it('should reorder hA2p4Py', () => {
        expect( reorder( 'hA2p4Py' ) ).toBe(  'APhpy24' )
    })

    it('should reorder m11oveMENT', () => {
        expect( reorder( 'm11oveMENT' ) ).toBe(  'MENTmove11' )
    })

    it('should reorder s9hOrt4CAKE', () => {
        expect( reorder( 's9hOrt4CAKE' ) ).toBe(  'OCAKEshrt94' )
    })

})
