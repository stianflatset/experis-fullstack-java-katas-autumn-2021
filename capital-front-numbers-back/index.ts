export function capsToFront( word ) {
    const source = [...word]
    const caps = []
    for ( let i = source.length - 1; i >= 0; i-- ) {
        if (source[i].match(/[A-Z]/)) {
            caps.push( source.splice(i, 1).pop() )
        }
    }
    return [...caps.reverse(), ...source]
}

export function numToBack( wordInArray ) {

    const source = [...wordInArray]
    const nums = []
    for ( let i = source.length - 1; i >= 0; i-- ) {
        if (source[i].match(/[0-9]/)) {
            nums.push( source.splice(i, 1).pop() )
        }
    }
    return [...source, ...nums.reverse()]
}

export function reorder( word ) {
    const movedCaps = capsToFront(word)
    const movedNums = numToBack( movedCaps )
    return movedNums.join('')
}
