export function removeSpecialCharacters(string) {
    return string.replace(/[^a-zA-Z' ]/g, '')
}

export function isAcquaintance(currentWords = {}, word = '') {
    return currentWords[word] === 3
}

export function isFriend(currentWords = {}, word = '') {
    return currentWords[word] === 5
}

export function noStranger(sentence = '') {

    const acquaintances = [],
        friends = []

    const words = removeSpecialCharacters(sentence.toLowerCase()).split(' ')

    words.reduce((acc, word) => {

        acc[word] = acc[word] + 1 || 1

        if (isAcquaintance(acc, word)) {
            acquaintances.push(word)
        }

        if (isFriend(acc, word)) {
            acquaintances.splice(acquaintances.indexOf(word), 1)
            friends.push(word)
        }

        return acc
    }, {})

    return [
        acquaintances,
        friends
    ]
}
