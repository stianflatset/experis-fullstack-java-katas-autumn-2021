# Experis Full Stack Java - Autumn 2021 Katas

Collection of morning excersices to get the brain working in the morning.

## Katas

1. 001-hello-world (Sample code)
2. Basket ball points
3. Double letter split
4. Capital letters front numbers back
5. Game of thrones titles
6. How many Boomerangs
7. Check password strength
8. Faro Shuffle
9. Stranger Danger
10. Elastic words
11. PigLatin
