export function numberOfBoomerangs( source ) {
    let boomerangs = 0
    for (let i = 0; i < source.length; i++) {
        if (isBoomerang(source.splice( i, 3))) {
            boomerangs += 1
        }
    }
    return boomerangs
}

export function isBoomerang(boomerang) {
    if (boomerang.length < 3) {
        return false
    }
    return boomerang[0] === boomerang[2] && boomerang[0] !== boomerang[1]
}
