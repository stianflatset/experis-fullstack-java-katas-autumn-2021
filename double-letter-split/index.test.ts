import { splitOnDoubleLetter } from './index'
describe('splitOnDoubleLetter', () => {

    it('should split letter', () => {
        expect(splitOnDoubleLetter('letter')).toEqual(['let', 'ter'])
    })
    
    it('should split really', () => {
        expect(splitOnDoubleLetter('really')).toEqual(['real', 'ly'])
    })
    
    it('should split happy', () => {
        expect(splitOnDoubleLetter('happy')).toEqual(['hap', 'py'])
    })
    
    it('should split shall', () => {
        expect(splitOnDoubleLetter('shall')).toEqual(['shal', 'l'])
    })
    
    it('should split tool', () => {
        expect(splitOnDoubleLetter('tool')).toEqual(['to', 'ol'])
    })
    
    it('should split mississippi', () => {
        expect(splitOnDoubleLetter('mississippi')).toEqual(['mis', 'sis', 'sip', 'pi'])
    })
    
    it('should split easy', () => {
        expect(splitOnDoubleLetter('easy')).toEqual([])
    })

    it('should split aaaa', () => {
        expect(splitOnDoubleLetter('aaaa')).toEqual(['a', 'a', 'a', 'a'])
    })
})

describe('Error handling', () => {
    it('should throw when undefined', () => {
        expect(() => splitOnDoubleLetter(undefined)).toThrow('No word was given')
    })
    
    it('should throw when empty string', () => {
        expect(() => splitOnDoubleLetter('')).toThrow('No word was given')
    })
    
    it('should throw when string with spaces', () => {
        expect(() => splitOnDoubleLetter('   ')).toThrow('No word was given')
    })
})