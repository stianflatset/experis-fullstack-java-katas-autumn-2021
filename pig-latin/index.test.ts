import {pigLatinSentence} from "./index";

describe('pigLatin', () => {

    it('should translate this is pig latin', () => {
        expect(pigLatinSentence("this is pig latin")).toBe("isthay isway igpay atinlay")
    })

    it('should translate wall street journal', () => {
        expect(pigLatinSentence("wall street journal")).toBe("allway eetstray ournaljay")
    })

    it('should translate raise the bridge', () => {
        expect(pigLatinSentence("raise the bridge")).toBe("aiseray ethay idgebray")
    })

    it('should translate all pigs oink', () => {
        expect(pigLatinSentence("all pigs oink")).toBe("allway igspay oinkway")
    })

})