export function pigLatinSentence(sentence) {

    let sentenceArray = sentence.split(' ');
    let pigLatinOutput = [];

    for (let i = 0; i < sentenceArray.length; i++) {
        let vowelIndex = sentenceArray[i].split('').findIndex(element => /[aeiou]/gi.test(element));
        if (vowelIndex === 0) {
            pigLatinOutput.push(sentenceArray[i] + 'way');
        } else {
            pigLatinOutput.push(sentenceArray[i].slice(vowelIndex, sentenceArray[i].length + 1) + sentenceArray[i].slice(0, vowelIndex) + 'ay');
        }
    }
    return pigLatinOutput.join(' ');

}

/**
 * @author Maximiliano Amodei
 * @link https://edabit.com/user/4oEq4bishsnov46yn
 */