export function multiplyByTwo(value) {
    return value * 2
}

export function multiplyByThree(value) {
    return value * 3
}

export function sum(twoPointerTotal, threePointerTotal) {
    return twoPointerTotal + threePointerTotal
}

export function basketballPoints(twoPointers, threePointers) {
    return sum(
        multiplyByTwo(twoPointers),
        multiplyByThree(threePointers)
    )
}