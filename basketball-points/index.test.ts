import { 
    multiplyByTwo, 
    multiplyByThree, 
    sum,
    basketballPoints
} from './index'

it('should mult 3 by 2', function() {
    expect( multiplyByTwo(3) ).toBe(6) 
})

it('should mult 1 by 3', function() {
    expect( multiplyByThree(1) ).toBe(3)
})

it('should add 6 to 3', function() {
    expect(sum(6, 3)).toBe(9)
})

describe('basketballPoints', function() {

    it('should ouput 5', function() {
        expect(basketballPoints(1, 1)).toBe(5)
    })

    it('should ouput 29', function() {
        expect(basketballPoints(7, 5)).toBe(29)
    })

    it('should ouput 100', function() {
        expect(basketballPoints(38, 8)).toBe(100)
    })

    it('should ouput 3', function() {
        expect(basketballPoints(0, 1)).toBe(3)
    })

    it('should ouput 0', function() {
        expect(basketballPoints(0, 0)).toBe(0)
    })


})