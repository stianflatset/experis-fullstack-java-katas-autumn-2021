export function split(arrayToSplit) {
  return [
    arrayToSplit.slice(0, arrayToSplit.length / 2),
    arrayToSplit.slice(arrayToSplit.length / 2),
  ];
}

export function shuffle(leftDeck, rightDeck) {
  const shuffled = [];
  for (let i = 0; i < leftDeck.length; i++) {
    shuffled.push(leftDeck[i]);
    shuffled.push(rightDeck[i]);
  }
  return shuffled;
}

export function matches(origin, shuffled) {
  return JSON.stringify(origin) === JSON.stringify(shuffled);
}

export function fillDeck(size) {
  return [...Array(size).keys()].map((i) => i + 1);
}

export function faro(deckSize) {
  const deck = fillDeck(deckSize);
  let shuffled = [...deck];
  let shuffleCount = 0;
  do {
    const [left, right] = split(shuffled);
    shuffled = shuffle(left, right);
    shuffleCount++;
  } while (matches(deck, shuffled) === false);
  return shuffleCount;
}
