/**
 * @author Truls Kippernes
 * @date 23 August 2021
 */

/**
 * Faro Shuffle
 * @param numberCards
 */
export function shuffleCount(numberCards){

    let faro = 2;
    let output = 1;
    while (faro !== 1) {
        output++;
        faro = 2 * faro % (numberCards - 1);
    }
    return output;
}